<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/{vue_capture?}', function () {
    return view('welcome');
})->where('vue_capture', '[\/\w\.-]*');

//Route::get('/companies','CompanyController@companies')->name('companies');
//Route::get('/company/{id}','CompanyController@company')->name('company');
//Route::get('/stations','StationController@stations')->name('stations');
//Route::get('/company-search','StationController@company_search')->name('company-search');
//Route::get('/radius-search','CompanyController@radius_search')->name('radius-search');

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/companies','CompanyApiController@companies');
Route::get('/company','CompanyApiController@company');
Route::get('/company/{id}','CompanyApiController@company');
Route::post('/company/add', 'CompanyApiController@create');
Route::put('/company/edit/{id}', 'CompanyApiController@update');
Route::delete('/company/del/{id}', 'CompanyApiController@remove');

Route::post('/company/get/{id}', 'CompanyApiController@get_stations');

Route::get('/stations','StationApiController@stations');
Route::get('/station/{id}','StationApiController@station');
Route::post('/station/add', 'StationApiController@create');
Route::put('/station/edit/{id}', 'StationApiController@update');
Route::delete('/station/del/{id}', 'StationApiController@remove');

Route::post('/stations/find', 'StationApiController@get_stations');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// test routes

Route::get('/testcompany','TestController@company');
Route::get('/testcompanies', 'TestController@companies');
Route::get('/testcompanyadd', 'TestController@testcompanyadd');


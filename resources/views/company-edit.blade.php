<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script
                src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    </head>
    <body>

    <div class="card">
    <div class="card-body">

    <form id="company_edit" class="text-center border border-light p-5" method="post">
        @csrf
        <p class="h4 mb-4">{{$company->name}}</p>

        <div id="form_message" class="alert alert-success d-none"></div>
        <div id="form_error" class="alert alert-danger d-none"></div>

        <input type="text" id="name" name="name" class="form-control mb-4" placeholder="Name" value="{{$company->name}}">

        <label>Parent Company</label>
        <select name="parent_company_id" class="browser-default custom-select mb-4">
            <option value="">Choose option</option>
            @foreach($companies as $comp)
            <option value="{{$comp->id}}" <?php if(isset($company->parent_company->id) && $company->parent_company->id == $comp->id){ echo 'selected';}?>>{{$comp->name}}</option>
            @endforeach
        </select>

        <button class="btn btn-info btn-block" type="submit">Edit</button>
    </form>

    </div>
    </div>

    <script>
        (function ($) {

            $('#company_edit').on('submit',function (event) {
                event.preventDefault();

                var name = $('#name').val();
                var parent = $('#parent_company_id').val();
                $.ajax({
                    type:'POST',
                    dataType: 'json',
                    contentType: "application/json",
                    url: "/api/company/edit/"+"{{$company->id}}",
                    data:{
                        "_token" : $('meta[name=_token]').attr('content'),
                        "name":name,
                        "parent_company_id":parent
                    },
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success:function (data) {
                        $('#form_error').addClass('d-none').text();
                        $('#form_message').removeClass('d-none').text(data.message);
                    },
                    error: function(data) {
                        $('#form_error').removeClass('d-none').text(data.message);
                        $('#form_message').addClass('d-none').text();

                        if (JSON.parse(data.responseText).errors.parent_company_id) {
                            $('#form_error').text(JSON.parse(data.responseText).errors.parent_company_id)
                        }
                    }
                })

            })
        })(jQuery);

    </script>
    </body>
</html>

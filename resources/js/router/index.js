import Vue from 'vue'
import Router from 'vue-router'
import Companies from '../components/Companies'
import Stations from '../components/Stations'
import getStations from '../components/getStations'
import getRadius from '../components/getRadius'

Vue.use(Router);

export default new Router({
    hashbang: false,
    linkActiveClass: "active",
    mode:'history',
    // base:__dirname,
  routes: [
      {
          path: '/companies',
          name: 'Companies',
          component: Companies,
      },
      {
          path: '/stations',
          name: 'Stations',
          component: Stations,
          // meta: { reuse: false }
      },
      {
          path: '/get-stations',
          name: 'getStations',
          component: getStations,
      },
      {
          path: '/get-radius',
          name: 'getRadius',
          component: getRadius,
      },
  ]
})


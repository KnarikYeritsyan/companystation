
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue'
import App from './App.vue'
import vueResource from 'vue-resource'
import router from './router'
import Vuex from 'vuex'
import VeeValidate from 'vee-validate'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/js/bootstrap.bundle.min'
import 'bootstrap/dist/js/bootstrap.min'
import 'jquery/dist/jquery.min'

Vue.use(VeeValidate);
Vue.use(vueResource);
Vue.use(BootstrapVue);
Vue.use(Vuex);
Vue.use( router );
require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.config.productionTip = false;
Vue.prototype.$ApiHost = 'http://127.0.0.1:8000'

/*Vue.component('navbar', require('./components/Navbar.vue'));
Vue.component('products', require('./components/products/Products'));
Vue.component('articles', require('./components/Articles.vue'));*/

new Vue({
    beforeCreate: function() {
        console.log(this.$ApiHost)
    },
    // el: '#app',
    router,
    components: { App },
    template: '<App/>'
}).$mount('#app');

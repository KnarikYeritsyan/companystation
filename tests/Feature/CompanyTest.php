<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CompanyTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testCompanies()
    {
        $company = factory('App\Company')->create();
        $response = $this->json('GET', '/api/testcompanies');
        $response
            ->assertStatus(200)
            ->assertJson(['status'=>'success']);
    }

    public function testCreateCompany()
    {
        $response = $this->json('POST', '/api/company/add',['name'=>'Company 1']);
        $response
            ->assertStatus(201)
            ->assertJson(['status'=>'success', 'message' => 'New Company Created Successfully.']);
    }

    public function testGetCompany()
    {
//        $company = factory('App\Company')->create();
        $response = $this->json('POST', '/api/company/add',['name'=>'Company 1']);
        $response = $this->json('GET', '/api/testcompany');
        $response
            ->assertStatus(200)
            ->assertJson(['name'=>'Company 1']);
    }

    public function testGetCompany1()
    {
        $company = factory('App\Company')->create();
        $response = $this->json('GET', '/api/testcompany');
        $response
            ->assertStatus(200)
            ->assertJson(['name'=>$company->name]);
    }

    public function testGetCompanyById()
    {
        $company = factory('App\Company')->create();
        $response = $this->json('GET', '/api/company/'.$company->id);
        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'success']);
    }

    public function testUpdCompany()
    {
        $company = factory('App\Company')->create();
        $response = $this->json('PUT', '/api/company/edit/'.$company->id,['name'=>'Company 1']);
        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'success']);
    }

    public function testDelCompany()
    {
        $company = factory('App\Company')->create();
        $response = $this->json('DELETE', '/api/company/del/'.$company->id);
        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'success']);
    }
}

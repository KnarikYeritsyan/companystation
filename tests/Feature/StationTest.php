<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class StationTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateStation()
    {
        $company = factory('App\Company')->create();
        $response = $this->json('POST', '/api/station/add',['name'=>'Station 1','latitude'=>'123.2114','longitude'=>'385932.235','company_id'=>$company->id]);
        $response
            ->assertStatus(201)
            ->assertJson(['status'=>'success']);
    }

    public function testGetStation()
    {
        $station = factory('App\Station')->create();
        $response = $this->json('GET', '/api/station/'.$station->id);
        $response
            ->assertStatus(200)
            ->assertJson(['name' => $station->name,'latitude'=>$station->latitude,'longitude'=>$station->longitude,'company_id'=>$station->company_id]);
    }

    public function testUpdStation()
    {
        $station = factory('App\Station')->create();
        $response = $this->json('PUT', '/api/station/edit/'.$station->id,['name'=>'Station 1','latitude'=>$station->latitude,'longitude'=>$station->longitude,'company_id'=>$station->company_id]);
        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'success']);
    }

    public function testDelStation()
    {
        $station = factory('App\Station')->create();
        $response = $this->json('DELETE', '/api/station/del/'.$station->id);
        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'success']);
    }
}

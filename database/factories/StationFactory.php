<?php

use Faker\Generator as Faker;

$factory->define(App\Station::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'latitude' => $faker->unique()->randomFloat('3'),
        'longitude' => $faker->unique()->randomFloat('3'),
        'company_id' => function () {
            return factory(App\Company::class)->create()->id;
        }
    ];
});

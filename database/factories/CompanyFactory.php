<?php

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'parent_company_id' => $faker->unique()->randomNumber(),
    ];
});

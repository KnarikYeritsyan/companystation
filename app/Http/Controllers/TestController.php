<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function companies()
    {
        $companies = Company::all();
        return response()->json(['status'=>'success', $companies],200);
//        return response()->json($companies,201);
//        return view('tasks.index',compact('tasks'));
    }

    public function company()
    {
        $company = Company::latest()->first();
        return response()->json($company,200);
    }

    public function testcompanyadd()
    {

    }
}

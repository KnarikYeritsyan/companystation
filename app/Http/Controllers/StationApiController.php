<?php

namespace App\Http\Controllers;

use App\Company;
use App\Station;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StationApiController extends Controller
{

    public function stations()
    {
        $companies = Company::all();
        $stations = Station::with('company')->orderBy('created_at','desc')->get();
        return response()->json(['status' => 'success', 'companies' => $companies,'stations'=>$stations],200);
//        return ArticleResource::collection($companies);
    }

    public function station($id)
    {
        $station = Station::where('id',$id)->with('company')->first();
        return response()->json($station,200);
    }

    public function create(Request $request)
    {
        $credentials = $request->all();

        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'latitude' => ['required', 'numeric'],
            'longitude' => ['required', 'numeric'],
            'company_id' => ['required', 'numeric'],
        ];

        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $company = Company::find($request['company_id']);
        if ($company) {
            Station::create([
                'name' => $request['name'],
                'latitude' => $request['latitude'],
                'longitude' => $request['longitude'],
                'company_id' => $request['company_id'],
            ]);
            return response()->json(['status' => 'success', 'message' => 'New Station Created Successfully.'], 201);
        }else{
            return response()->json(['status' => 'error', 'message' => 'Company with given id does not exist.'],404);
        }
    }

    public function update($id,Request $request)
    {
        $credentials = $request->all();

        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'latitude' => ['required', 'numeric'],
            'longitude' => ['required', 'numeric'],
            'company_id' => ['required', 'numeric'],
        ];

        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $company = Company::find($request['company_id']);
        if ($company) {
            Station::find($id)->update([
                'name' => $request['name'],
                'latitude' => $request['latitude'],
                'longitude' => $request['longitude'],
                'company_id' => $request['company_id'],
            ]);
            return response()->json(['status' => 'success', 'message' => 'Station Updated Successfully.'], 200);
        }else{
            return response()->json(['status' => 'error', 'message' => 'Company with given id does not exist.'],404);
        }
    }

    public function remove($id)
    {
        $station = Station::find($id);
        if ($station){
            $station->delete();
            return response()->json(['status' => 'success', 'message' => 'Station Deleted Successfully.'],200);
        }else{
            return response()->json(['status' => 'error', 'message' => 'Station with given id does not exist.'],404);
        }
    }

    public function get_stations(Request $request)
    {
        $credentials = $request->all();

        $rules = [
            'latitude' => ['required', 'numeric'],
            'longitude' => ['required', 'numeric'],
            'radius' => ['required', 'numeric'],
        ];

        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $lat = $request['latitude'];
        $lng = $request['longitude'];
        $radius = $request['radius'];
        $query = "SELECT id, name, latitude, longitude, ( 6371 * acos( cos( radians(".$lat.") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( latitude ) ) ) ) AS distance FROM stations HAVING distance < ".$radius." ORDER BY distance;";
        $locations = \DB::select($query);
        return response()->json(['status' => 'success', 'stations' => $locations],200);
    }
}

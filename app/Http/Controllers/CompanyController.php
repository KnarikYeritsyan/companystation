<?php

namespace App\Http\Controllers;

use App\Company;
use App\Station;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{

    public function companies()
    {
        $companies = Company::with('parent_company')->get();
        return view('company-list',compact('companies'));
    }

    public function company($id)
    {
        $companies = Company::all();
//        $company = $id->with('parent_company')->get();
        $company = Company::where('id',$id)->with('parent_company')->first();
        return view('company-edit',compact('company','companies'));
    }

    public function create(Request $request)
    {
        $credentials = $request->all();

        $rules = [
            'name' => ['required', 'string', 'max:255'],
        ];

        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (isset($request['parent_company_id']) && $request['parent_company_id'] != ''){
            $company = Company::find($request['parent_company_id']);
            if ($company){
                Company::create([
                    'name'=>$request['name'],
                    'parent_company_id'=>$request['parent_company_id']
                ]);
                return response()->json(['status' => 'success', 'message' => 'New Company Created Successfully.'],200);
            }else{
                return response()->json(['status' => 'error', 'message' => 'Company with given id does not exist.'],404);
            }
        }else{
        Company::create([
            'name'=>$request['name'],
            'parent_company_id'=>isset($request['parent_company_id'])?$request['parent_company_id']:Null
        ]);
        return response()->json(['status' => 'success', 'message' => 'New Company Created Successfully.'],200);
        }
    }

    public function update($id,Request $request)
    {
        $credentials = $request->all();

        $rules = [
            'name' => ['required', 'string', 'max:255'],
        ];

        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (isset($request['parent_company_id'])) {
            $company = Company::find($request['parent_company_id']);
            if ($company) {
                Company::find($id)->update([
                    'name' => $request['name'],
                    'parent_company_id' => $request['parent_company_id']
                ]);
                return response()->json(['status' => 'success', 'message' => 'Company Updated Successfully.'], 200);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Company with given id does not exist.'], 404);
            }
        }else{
            Company::find($id)->update([
                'name' => $request['name'],
                'parent_company_id' => isset($request['parent_company_id']) ? $request['parent_company_id'] : Null
            ]);
            return response()->json(['status' => 'success', 'message' => 'Company Updated Successfully.'], 200);
        }
    }

    public function remove($id)
    {
        $company = Company::find($id);
        if ($company){
            $company->delete();
        return response()->json(['status' => 'success', 'message' => 'Company Deleted Successfully.'],200);
        }else{
            return response()->json(['status' => 'error', 'message' => 'Company with given id does not exist.'],404);
        }
    }

    public function get_stations($id)
    {
//        $company = Company::where('id',$id)->with('grandchildren')->with('station')->get();
        $company = Company::where('id',$id)->with('station')->first();
        if (!$company){
            return response()->json(['status' => 'error', 'message' => 'Company with given id does not exist.'],404);
        }

        $companies = Company::with('station')->get();
        $tree = $this->buildTree($companies,$id);
        $ids = explode(',',$tree);
        $ids = array_values(array_filter($ids, 'strlen'));
        $stations = $company->station;
        foreach ($ids as $i){
            $stations[] = Station::find($i);
        }

        return response()->json(['status' => 'success', 'company_stations' => $stations],200);
    }

/*    function buildTree($elements, $parentId = 1) {
        $branch = [];
        $stat = [];

        foreach ($elements as $element) {
            if ($element->parent_company_id == $parentId) {
                $children = $this->buildTree($elements, $element['id']);

                if ($children) {
                    $element->children = $children;
                    $stat[] = $children;
                }

                $branch[] = $element;
            }
        }

        return $branch;
    }*/

    function buildTree($elements, $parentId) {
        $branch = '';
        foreach ($elements as $element) {
            if ($element->parent_company_id == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $branch .= $children.',';
                }
                if (!empty($element->station)){
                    foreach ($element->station as $item)
                    {
                        $branch .= $item->id.',';
                    }
                }
            }
        }
        return $branch;
    }

}

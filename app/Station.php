<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Station extends Model
{
    use Notifiable;

    protected $fillable = [
        'name','latitude','longitude','company_id'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }
}

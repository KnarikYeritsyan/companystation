<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Company extends Model
{
    use Notifiable;

    protected $fillable = [
        'name','parent_company_id'
    ];

    public function station()
    {
//        return $this->hasOne('App\Station','company_id');
        return $this->hasMany('App\Station','company_id');
    }

    public function child_company()
    {
//        return $this->hasOne(self::class,'parent_company_id')->with('station');
        return $this->hasMany(self::class,'parent_company_id')->with('station');
    }

    public function parent_company()
    {
        return $this->belongsTo(self::class,'parent_company_id','id');
    }

    public function grandchildren()
    {
        return $this->child_company()->with('grandchildren');
    }

}
